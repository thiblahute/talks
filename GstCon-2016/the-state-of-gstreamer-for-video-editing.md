<!-- $theme: gaia -->
<!-- page_number: true -->

## History of multimedia editing with GStreamer

* GNonlin:

    * Started in 2001 by Wim Taymans
    * In 2004 Edward Hervey starts hacking on it when starting the Pitivi project

* GStreamer Editing Services:

    * Started in 2009 by Edward Hervey

* Non Linear Engine

    * Big refactoring of GNonLin in 2014

## The NLE GStreamer plugin

![nlecomposition](NleComposition.jpg "Time based pipeline sequencing")


## Major issues in GNonLin

* Lack of thread safety in Gnonlin
* **Many** useless threads were created

## Lack of thread safety in GNonLin

- gnlcomposition used to unlink/relink new `pipelines` from the streaming thread (where the EOS was received) or from the seeking thread
- In nlecomposition we introduced a new `master` thread where all the operations on children happen sequentially


## Creation of many useless thread

- In GnlComposition we used to have all elements inside in `PAUSED` state → **many** thread created and just waiting
- In NleComposition elements are **not** inside it until they are actually needed (and are in `READY` state only)

## NLE state

* Working quite well when used with GES
* Most races are gone
* Still lack testing outside GES
* Some part of the code could use refactoring
* Still lacking gst-launch support

## Goals of GES

* Allow simple editing to get done as easily as possible
* Allow full fledge Video editing app to be built 'simply'

* **NON** goal of GES: handle all use cases where NLE is useful

## Main features

* Create a timeline, add clips, titles, transitions, effects, group them... easily
* `GstPipeline` subclass to play and render a timeline
* Edit the clips with advanced methods (ripple, roll, etc...)
* Asset handling support
* `ges-launch-1.0` a tool to use GES on the command line

## ges-launch got rewriten

* New 'command line formatter'
* Pretty flexible syntax that allows to virtually define any timeline

## Playback a portion of a media file

``` c++
ges-launch-1.0 +clip /path/to/media \
  inpoint=4.0 duration=2.0 start=4.0
```

## Render two parts of media files stitched

``` bash
ges-launch-1.0 \
  +clip /path/to/media
    inpoint=4.0 duration=2.0 \
  +clip /path/to/media1 \
    inpoint=5.0 start=2.0 duration=1.0 \
  -f 'video/webm:video/x-vp8:audio/x-vorbis' \
  -o /path/to/rendered/file
```

## GstValidate integration

* GstValidateScenario: straight forward files to make actions on a pipeline, or any component

* Use to easily reproduce bugs (those scenario files are generate by Pitivi)

## Scenario example

``` bash
add-clip, name=c0, layer-priority=0,\
  asset-id=file:///some/video, type=GESUriClip, \
  start=0, inpoint=0, duration=5.0

# Adding a clip to a second layer
add-clip, name=c1, layer-priority=1,\
  asset-id=file:///other/video, type=GESUriClip, \
  start=0, inpoint=0, duration=5.0

# Trim the clip
edit-container, edge=edge_start, container-name=c0, \
  position=1.0, edit-mode=(string)edit_trim;
```

## 

## testing

* Quite many unit tests
* Full integration testsuite running on ci.gstreamer.net playing seeking and rendering timelines
