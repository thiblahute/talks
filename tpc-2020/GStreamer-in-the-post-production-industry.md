---
theme: night
transition: 'fade'
revealOptions:
    transition: 'fade'

title: "The motion picture industry and open source software: GStreamer as an
alternative"
author: Thibault Saunier
keywords:
    - video editing
    - open source
    - post production
    - multimedia framework
    - GStreamer
abstract: >
    Open source methodologies and technologies are
    driving modern software development in many industries and the post
    production industry is part of that move in many aspects. But a lot more can
    be done to leverage the potential of all the software that exists out there
    and is used be millions of consumers.

    This talk will explain how and why GStreamer, the multimedia framework that
    powers most TV screens and set top boxes in the world and is at the core of
    leading Deep learning frameworks such as the nvidia DeepStream SDK, could be
    leveraged in the motion picture industry to allow faster innovation and solve
    issues by reusing all the multi-platform infrastructure the community has to
    offer.
---

<style>
.container{
    display: flex;
}
.col{
    flex: 1;
}
</style>


# GStreamer in the post production industry

<!-- .slide: data-background="https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/PiTiVi_Logo.svg/1200px-PiTiVi_Logo.svg.png" class="with_background" -->

Thibault Saunier


---

# Who am I ?

- Multimedia Software engineer at Igalia

- Core GStreamer developers for 10 years
  - Lead developer of the GStreamer Editing Services
  - Maintainer of the Pitivi video editing application

---

# What is this about?

- What is GStreamer?
- Where is it used?
- GStreamer in post production pipelines

----

# What is GStreamer ?

- Flexible multimedia framework
- Open Source LGPL v2.1
    - Proprietary applications and plugins allowed
- Cross platform
    - Binaries for Windows, OSX, iOS and Android
- Installed by default on all Linux distributions
- Stable C API and ABI

Note:
- ugly: possibly GPL
- 1.0 release in 2012

----

# Language bindings

- python (official)
- rust (official)
- c-sharp (official)
- .Net (official)
- Java (contrib)
- Go (contrib)
- ...

----

# GStreamer architecture <!-- .element class="with_background" -->

![](media/GStreamer_overview.svg)

----

# Plugin centric design

* Plugins provide the elements
* Over 250 plugins
* More than 1700 elements


----

# Pipeline based design

![](media/simple-player.png)

---

# What it is not

- A monolitique multimedia library
- A player
- A streaming / rendering server
- A video editing tool

---

# Where is it used currently ?

----

## The Web

- Web Engines
    - WebKit ![small_icon](media/WebKit.png)
    - Servo ![small_icon](media/servo.png)

- The web in GStreamer
    - GstWPE
    - GstWebRTC

----

## Embedded devices

- Set top boxes
- Low powered embedded devices

Notes:
- Basic block for many Linux Sdks

----


## TVs and phones <!-- .element style="position: fixed; top: 0.0em; left: 0.5em" -->

<!-- ![](media/tvs.png) -->

<!-- .slide: data-background="media/tvs.png" -->

----

## Integrated Entertainment Systems <!-- .element class="with_background" -->

Note:
- In flight entertainment
- In-vehicle infotainment (IVI)
- Video-on-demand
- Sharing media between users
- Public announcements
- Synchronized multi-device playback

<!-- .slide: data-background="media/in-flight.jpg" -->

----

## Video walls
## & distributed audio systems

<!-- .slide: data-background-video="media/videowall-noaudio.mp4" class="with_background" -->

Notes:

- Frame-accurate synchronized output–Also for audio
- Control & command rooms
- Digital signage

----

- Video Security Systems
- Augmented/Virtual reality platforms
- Drones
- Industrial image processing
- Cloud web services

![](media/amazon-alexa.png)

Notes:
- WebRTC conferencing
- Proprietary solutions for video conferencing

----

# Deep Learning frameworks and Sdks


<!-- .slide: data-background="media/machine-learning.jpg" class="with_background" -->

- NVidia DeepStream
- Samsung NNStreamer
- GstInference

Notes:
- GstInference aims integrating other deep learning frameworks into GStreamer

----

# Ingest and playout servers

<!-- .slide: data-background="media/City_tv_control_room_Doors_Open_Toronto_2012.jpg" class="with_background" -->

- Live recording, compositing
- Broadcasting
- Extreme reliability 24/7 availability

----

# Video editing software

(Less than 50K lines of python code)

<!-- .slide: data-background="media/Pitivi.png" class="with_background" -->

Notes:
- Pitivi
- Almost 1.0
- Started in 2004
- Started a long time and several GStreamer developers worked on it
- GStreamer Editing Services !

---

# GStreamer in film making pipelines

- NLE features have been built since 2002
- Made to integrate with existing tools

Note:
- Toolbox

----

# Integration with GPUs

- OpenGL
- Vulkan
- Direct3D

----

# Support formats from the industry

- OpenEXR
- Prores
- MXF
- ...

----

# GStreamer Editing Services

- High level APIs for video editing
    - GESTimeline
    - GESProject
    - GESLayer
    - GESTrack
    - GESAsset

NOTE:
- Written in C with Python/Javascript/rust bindings

----

# OpenTimelineIO

- Interchange format for editorial cut information
- ASWF project

Note:
- Open Source API and interchange format that facilitates collaboration and
  communication of editorial data and timeline information between a studio’s
  Story, Editorial, and Production departments all the way through
  Post-Production

----

# Integration with OpenTimelineIO


``` bash
gst-play-1.0 /some/project.fcpxml
```

- First class citizen in GStreamer

Note:
- Any project can be played/rendered as any media file


---

# Thanks!

Questions ?