---
title: "The motion picture industry and open source software: GStreamer as an alternative"
author:
    - Thibault Saunier
urlcolor: cyan
keywords:
    - video editing
    - open source
    - post production
    - multimedia framework
    - GStreamer
abstract: |
    Open source methodologies and techologies are driving modern software development
    in many industries and the post production industry is part of that move in many
    aspects. But a lot more can be done to leverage the potential of all the software
    that exists out there and is used be millions of consumers.

    This talk will explain how and why GStreamer, the multimedia framework that
    powers most TV screens and set top boxes in the world and is at the core of
    leading Deep learning frameworks such as the nvidia DeepStream SDK, could be
    leveraged in the motion picture industry to allow faster innovation and solve
    issues by reusing all the multi-platform infrastructure the community has to
    offer.
...

\pagebreak

Historically, the motion picture industry worked with studio specific tools and
methodologies using proprietary software and without sharing much technical
knowledge.  However, with the rise of Open Source, this has been changing for a
while, with trends increasing faster since 2018 when the Linux Foundation
started the Academy Software Foundation (aswf).

We, the developers of the [GStreamer] multimedia framework, have been working on
a piece of technology that is ready to be used and adapted for any kind of video
processing and content creation use case. We believe that this can be the
building block that the industry needs, and we aim to create a bigger community
of companies contributing, caring and using Gstreamer in this sector.

[GStreamer]: https://gstreamer.freedesktop.org

## The GStreamer framework

[GStreamer] is a powerful and versatile open source multimedia framework that
enables the creation of any kind of media applications. It is deployed in many
industries, ranging from TVs or tiny embedded devices to rendering farms or
broadcasting services. Moreover, it is used as the basis of Deep learning SDKs
such as nvidia's [DeepStream] or Samsung's [nnstreamer].  It has over 1550 all
time contributors (~270 per year in the last years).

GStreamer tries to leveraging existing libraries and tools to perform media
processing instead of re-implementing the algorithms on its own. Its
architecture allows application writers to use those algorithms in a
comprehensive and efficient way. All in all, the GStreamer framework focuses on
offering both powerful and user friendly APIs for all kind of multimedia needs.

GStreamer is multiplatform and official builds are provided for Windows, MacOS,
Android, and iOS, and is installed by default on all major Linux distributions.
This makes GStreamer a very convenient tool to write multiplatform applications,
including post production related ones.

GStreamer provides an impressive number of algorithms and protocol
implementations: there are 250 official plugins and [1500 additional third party
ones](https://gstreamer.freedesktop.org/documentation/plugins_doc.html?gi-language=python)
which makes a complete toolbox to facilitate and speed up solving any kind
of problem you may face.

Moreover the comunity officially provides bindings for many languages such as
Rust, Python, c# and JavaScript so developers can focus on solving their issues
without needing to learn new languages.

[Deepstream]: https://developer.nvidia.com/deepstream-sdk
[nnstreamer]: https://nnstreamer.ai/

## GStreamer and post production

Since the projects very beginning, the framework was designed having post
production use cases in mind and there have been functionalities introduced
quite early in the GStreamer project history related to video editing. For
instance, all the way back in 2002  GNonLin was created to provide the basic
building block for all Non Linear video editing requirements. Later, in 2009, a
new component called [GStreamer Editing Services], GES, was created with the
goal of providing a set of data structures and services in the domain of video
editing. In particular, GES was designed with the idea of providing simple,
flexible and high level APIs and trying to cover aspects like timelines, clips,
layers, assets, etc

It is also important to emphasize that GStreamer supports many formats widely
used in the post production industry, such as MXF, open EXR, prores, etc.. and
this makes it an important asset. More still: it is easy to program new plugins
with GStreamer to add support for anything that you need. Finally, regarding
compatibility, we outline that GStreamer provides native integration with
[OpenTimelinIO] and, therefore, it supports all the editorial cut information
formats supported by this project (EDL, FCP7/X, AAF, RV, ALE, ...).
At this moment, we can state that GStreamer is a stable and mature technology
for use in the post production field.  As a demonstration of this, the [Pitivi]
open source video editing application was developed leveraging GStreamer. It is
a full feature video editing application and has less than 30K lines of python
code. On top of this, it is also fully customizable and can be extended easily
with new features.

[GStreamer Editing Services]: https://gstreamer.freedesktop.org/documentation/gst-editing-services/?gi-language=python
[Pitivi]: http://www.pitivi.org/
[OpenTimelineIO]: https://opentimelineio.readthedocs.io/en/latest/