## WebRTC in WebKitGtk and WPE

![WebRTC icon](images/webrtc.jpg)
![WebKit icon](images/webkit.png)


Thibault Saunier

---

## Short history

<!-- With Ericson -->
* OpenWebRTC based backend around 2015 and 2016
* In 2016 Apple developed a LibWebRTC based backend
* In 2017 the OpenWebRTC project was abandoned
* OpenWebRTC was removed a year ago

---

## New implementation

* `LibWebRTC` for the network
* `GStreamer` for the media processing

![WebRTC icon](images/webrtc.jpg)
![GStreamer icon](images/GStreamer_x_200.jpg)
![WebKit icon](images/webkit.png)

---

# Why LibWebRTC?

* Mature and stable
* Feature complete
* Very active
* Big parts were implemented for Apple ports

---

# Why GStreamer?

* Leverage GStreamer hardware integration
+ Same code paths for all the multimedia processing
* Use `webrtcbin` GStreamer element in the future ?

---

# WebRTC related APIs

* `MediaDevices.EnumerateDevices()`
* `MediaDevices.GetUserMedia()`
* `MediaDevices.MediaStream`
* `RTCPeerConnection`

Note:
* appr.tc is working
* We got a 140 layout tests enabled

---

![Dataflow](images/webkitwpe_webrtc_media_dataflow.png)

---

# The future

* More stabilization
* Optimise for embedded device use cases (WPE targets)
* Implement missing APIs and features
* Enable WebRTC on release builds

Note:
* WebAudio / canvas capture
* Enable web platform tests
* jitsi
* Screen sharing for example

---

# Questions?