#!/bin/sh

reveal-md OpenTimelineIO-GStreamer-integration.md --css igalia.css -g --static OpenTimelineIO-GStreamer-integration/ && cp -R medias/ OpenTimelineIO-GStreamer-integration/
decktape --size=1920x1080 reveal file://$PWD/OpenTimelineIO-GStreamer-integration/index.html OpenTimelineIO-GStreamer-integration.pdf