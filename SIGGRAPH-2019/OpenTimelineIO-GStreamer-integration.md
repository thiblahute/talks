---
title: OpenTimelineIO and GStreamer integration
theme: night
---

<style>
.container{
    display: flex;
}
.col{
    flex: 1;
}
</style>

<h2 style="position: fixed; top: 1.0em">
OpenTimelineIO
<br/>
and
<br/>
GStreamer integration
</h2>

<div style=" position: fixed; bottom: 1.0em; right: -2.0em">
    Thibault Saunier
<div>

---

## GStreamer

- Cross platform open source pipeline based multimedia framework
- By default on all Linux distributions
- Used in many industries

NOTE:
- Embedded devices/TVs
- Broadcasting industry
- Medical industry
- ...

---

## GStreamer Editing Services

- High level APIs for video editing

    - GESTimeline
    - GESProject
    - GESLayer
    - GESTrack

NOTE:
- Written in C with Python/Javascript/rust bindings 

---

### OpenTimeline IO integration

- Added an `xges` OpenTimelineIO adapter

  → We can convert editorial cut information from and to the GES serialization format

---

### Support in GES

- `GESOtioFormatter`

  → Any format supported by OpenTimelineIO can be loaded in GES

  → GES timelines can be serialized in any format supported by otio

---

### Support in GES

<h4 style="text-align:left">Playback example</h4>

``` bash
ges-launch-1.0 -l foo.otio
```

<h4 style="text-align:left">Rendering example</h4>

``` bash
ges-launch-1.0 -l foo.otio -o rendered_foo.mkv \
    --format video/x-matroska:video/x-av1:audio/x-opus
```

<h4 style="text-align:left">Convertion example</h4>

``` bash
# foo.xml is a Final Cut Pro XML file
ges-launch-1.0 -l foo.xml --save foo.otio
```

NOTE:
  - Implemented in python using PyGObject
  - Plans to port it to the C++ API when possible

---

### Support in GStreamer

- Wrote the `gesdemux` GStreamer element
- Implemented `typefind` function to detect file formats supported by Otio

→ Files supported by OpenTimelineIO can be used as any media files in GStreamer

``` bash
gst-launch-1.0 playbin uri=file:///foo.otio
```

NOTE:
- Implemented in python using PyGObject
- Planned to be ported to the C++ API when ready

---
#### Support in Pitivi

![](medias/PitiviFCPX.png)

---

### Future

- Move to the Otio C++ API
- Add support for nested timelines in the Otio adapter

---

Thanks!