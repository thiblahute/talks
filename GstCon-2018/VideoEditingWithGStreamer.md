---
title: Video Editing with GStreamer
theme: night
revealOptions:
    transition: 'fade'
---

<style>
.container{
    display: flex;
}
.col{
    flex: 1;
}
</style>

<h2 style="position: fixed; top: 1.0em">
Video Editing with GStreamer
</h2>

<!-- .slide: data-background="https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/PiTiVi_Logo.svg/1200px-PiTiVi_Logo.svg.png" -->

<div style=" position: fixed; bottom: 1.0em; right: -2.0em">
    Thibault Saunier
<div>

---

## Architecture

---

## NLE: Non Linear Engine

- Set of GStreamer element
- Low level primitives

Note:
- Started by Wim Tayman in 2001
- Then developed and maintained by Edward Hervey starting in 2004 - for Pitivi

---

### nlecomposition

![nlecomposition](medias/NleComposition.jpg)

---

## GStreamer Editing Services

- Higher level APIs
- The concepts match video editing applications

---

### GES concepts

* GESProject: Contains the GESAssets, serializable
* GESTimeline: Contains the tracks, audio or video
* GESLayer: Contains the GESClips

Note:
* Layers govern the painting order

---

### Notable GES features

* Keyframes: a timestamp used that indicates the beginning or end of a change made to a property
* Proxy: intermediary media files to optimize editing

<!-- ![](medias/GESConcepts.png) -->

---

## GES bindings

* GObject introspection:
    * python
    * javascript (gjs)
* C#
* Rust? Planned

Note:
* Nugget database

---

## GES documentation

* Hotdoc based documentation in the pipeline
* Reworked structure while porting
* Python examples, more to be written!

---

## Pitivi

<!-- .slide: data-background-image="medias/TowardPitivi1.0.png" -->

Note:
* 50 000 LOC
* Python
* Started in 2004... we are **finally** reaching 1.0

---

## What is missing for 1.0

Nothing... featurewise

- 10 bugs open
    - 2 complex ones
    - 5 with patches in review

---

## Demo

Note:
* Add clip
* Timeline editing
* split
* Transition
* title
* Proxy

---

## Many features for the near future

Current `master` branch will eventually become 2.0

---

## Plugins support

![](medias/freesound-preview.gif)

Thanks to Fabián Orccón 2017 GSoC

---

### Rich Effects configuration

 <iframe width="1109" height="500" src="https://www.youtube.com/embed/bHn8SVQJFlQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Thanks to Suhas Nayak 2017 GSoC

---

### Better UX

![Better UX](medias/screenshot-from-2018-07-21-00-29-40.png)

"DONE" - Harish Fulara during his 2018 GSoC

Note:
* Nice welcoming window
* Optimise space used by the viewer

---

### Scaled proxies support

![](medias/scaledProxies.png)

Thanks to Yatin Maan during his 2018 GSoC

---

## The future

* Speed rate control with motion ramping
* Proper GStreamer GL integration and testing
* EDL(s) / OpenTimelineIO support
* Integration with other video editing related softwares
* Subprojects inside Pitivi (aka. timeline inside timeline)

---

## Thank you!

Questions?