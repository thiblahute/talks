# Video Editing with GStreamer, status update and plans for the future

Pitivi 1.0, the main GStreamer based Video editing application, is around
the corner, we have been working in the last few years on stabilizing
key areas of GStreamer to have a solid back-end for Video Editing.

This talk will first present the various GStreamer components we heavily
rely on in Pitivi and the new "features" that have been added recently
to enhance the robustness of the application.

A long term goal of Pitivi is to support professional use cases,
being based on GStreamer means that our backend is very flexible
and already supports important pro editing technologies. Still, we
have quite a long way to go to properly support that target audience
and this talk will focus on the challenges we need to overcome to
reach that goal.