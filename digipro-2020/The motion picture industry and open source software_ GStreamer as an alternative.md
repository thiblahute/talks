<!-- Abstract:

# The motion picture industry and open source software: GStreamer as an alternative

Open source metodologies and techologies are driving modern software development
in many industries and the post production industry is part of that move in many
aspects. But a lot more can be done to leverage the potential of all the software
that exists out there and is used be millions of consumers.

This talk will explain how and why GStreamer, the multimedia framework that
powers most TV screens and set top boxes in the world and is at the core of
leading Deep learning frameworks such as the nvidia DeepStream SDK, could be
leveraged in the motion picture industry to allow faster inovation and solve
issues by reusing all the multiplatform infrastructure the community has to
offer.
-->

---
urlcolor: cyan
author:
- Thibault Saunier
table-of-contents: true
template: letter
...

# The motion picture industry and open source software: GStreamer as an alternative

Historically the motion picture industry has been working with studio specific
tools and methodologies using proprietary software and without sharing much
technical knowledge. With the rise of Open Source in the software industry this
has started to change a while back and this trends is going faster since 2018
when the Linux Foundation started the Academy Software Foundation (aswf).

We, the developers of the [GStreamer] multimedia framework, have been working on
a piece of technology that is ready to be used and adapted for any kind of video
processing and content creation use case. So, we believe that it can be the
building block that this industry needs and we aim to create a bigger community
of companies contributing, caring and using Gstreamer in this sector.

[GStreamer]: https://gstreamer.freedesktop.org

## The GStreamer framework

[GStreamer] is a powerful and versatile open source multimedia
framework that allows creating any kind of media applications. It is deployed in
many industries, ranging from TVs or tiny embedded devices to rendering farms
or broadcasting services. Moreover, it is used as the basis of Deep learning
Sdks such as nvidia' [DeepStream] or Samsung' [nnstreamer] and has over 1550 all
time contributors (~270 per year in the last years).

GStreamer does not implement the media processing algorithms but instead
leverages existing libraries or tools through dedicated plugins. This
architecture allows application writters to use them all in a comprehensive and
efficient way. All in all, the GStreamer framework focuses on offering both
powerful and user friendly APIs for all kind of multimedia needs.

GStreamer is multiplatform and official builds are provided for windows, macOS,
android, iOS, apart from being installed by default on all major linux
distributions. This makes GStreamer a very convenient tool to write
multiplatform applications, including post production related ones.

GStreamer provides an impressive number of algorithms and protocol
implementations: there are over [1500
elements](https://gstreamer.freedesktop.org/documentation/plugins_doc.html?gi-language=python)
provided by the community which make a complete toolbox to facilitate and speed
up solving any kind of problem you may face.

Moreover the comunity officially provides bindings for many languages such as
rust, python, c# and javascript so developers can focus on solving their issues
without needing to learn new languages.

[Deepstream]: https://developer.nvidia.com/deepstream-sdk
[nnstreamer]: https://nnstreamer.ai/

## GStreamer and post production

Since the beginning of the project the framework was designed having post
production use cases in mind and functionalities related to video editing were
introduced quite soon in the GStreamer project history. For instance, already
back in 2001  GNonLin was created to provide the basic building block for all
Non Linear video editing requirements. Later, in 2009, a new component called
[GStreamer Editing Services], GES, was created with the goal of providing a set
of data structures and services in the domain of video editing. In particular,
we highlight that GES was designed with the idea of providing simple, flexible
and high level APIs and trying to cover aspects like timelines, clips, layers,
assets, etc

It is also important to emphasize that GStreamer supports many formats widely
used in the post production industry, such as MXF, open EXR, prores, etc.. and
this makes it an important asset. Additionally, and what matters more, it is
easy to program new plugins with GStreamer to add support for anything you need.
Regarding compatibility, we outline that GStreamer provides native integration
with [OpenTimelinIO] and, therefore, it supports all the editorial cut
information formats supported by it (EDL, FCP7/X, AAF, RV, ALE, ...).

At this moment, we can state that GStreamer is a stable and mature technology to
use in the post production field and, as a demostration of it, we would like to
mention that the [Pitivi] open source video editing application was developed
leveraging GStreamer. It is a full featured video editing application and has
less than 30K lines of python code. On top of this, it is also fully
customizable and can be extended easily with new features.

[GStreamer Editing Services]: https://gstreamer.freedesktop.org/documentation/gst-editing-services/?gi-language=python
[Pitivi]: http://www.pitivi.org/
[OpenTimelineIO]: https://opentimelineio.readthedocs.io/en/latest/