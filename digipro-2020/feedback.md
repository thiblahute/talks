Thibault,

Thank you for your submission (The motion picture industry and open source software: GStreamer as an alternative) to DigiPro2020. Unfortunately, we will not be including it in this year’s conference.

Due to the unusual circumstances we find ourselves in, we are looking to run a slightly shorter program this year and have had to be particularly selective about the content we accepted.

We hope you will consider submitting again to a future DigiPro conference, and will still join us for the virtual event this year.

For your reference, we have included the feedback from the reviewers.

Best regards,

Trina Roy & Rob Pieké
Program Co-Chairs, DigiPro 2020

SUBMISSION: 6
TITLE: The motion picture industry and open source software: GStreamer as an alternative


----------------------- REVIEW 1 ---------------------
SUBMISSION: 6
TITLE: The motion picture industry and open source software: GStreamer as an alternative
AUTHORS: Thibault Saunier

----------- Overall evaluation -----------
SCORE: 0 (borderline paper)
----- TEXT:
I'm a little unclear what this talk would cover. The submission itself gives an overview of GStreamer - will the talk itself be a technical guide to developing tools with GStreamer, comparisons to alternate frameworks, case studies of where the industry has used GStreamer?

I suspect GStreamer is of great potential benefit, but I'm not quite sure what sort of tools could benefit from it. I can't tell if GStreamer and/or existing software that's built on it are potential out-of-the-box solution to these, or whether implementing a tool would require significant development effort.

Ideally, such a talk would be co-presented by Motion Picture/VFX content creator and/or VFX software vendor. Without that, and without a more in-depth paper to review, I'm concerned there would be a disconnect between the talk and its audience.

----------------------- REVIEW 2 ---------------------
SUBMISSION: 6
TITLE: The motion picture industry and open source software: GStreamer as an alternative
AUTHORS: Thibault Saunier

----------- Overall evaluation -----------
SCORE: 1 (weak accept)
----- TEXT:
This submission proposes to illustrate how to employ GStreamer in various VFX workflows. Regrettably is quite apparent the author has minimal familiarity with the problems of our industry, which makes the writeup not as strong as it could be. However it seems to me the library being quite mature, it would be useful that the audience was aware of what it had to offer.



----------------------- REVIEW 3 ---------------------
SUBMISSION: 6
TITLE: The motion picture industry and open source software: GStreamer as an alternative
AUTHORS: Thibault Saunier

----------- Overall evaluation -----------
SCORE: 0 (borderline paper)
----- TEXT:
This submission seems like a plea to use gstreamer without presenting ideas or examples of what is possible as an upside, and needs significant improvement prior to publication / presentation.

The overview of gstreamer, and how it is using OpenTimelineIO and OpenEXR from ASWF, and how that all relates to production, serves as an introduction. That is great, but this does not provide a rational why a facility, or even the industry, might switch.

So to bolster that, I think this paper should consider adding a few things to present as brief thought exercises or did-you-know-we-could-do-this:
- A brief technical overview (block diagram) of the library, what are the big components, and how they fit together / provide something other libraries do not. perhaps the plugin cache architecture is unique relative to other systems? or maybe that overview plus how few lines it takes to integrate?
- A bit of a deep dive into one of the unique features of gstreamer to use as a selling point. A prime example of that might be the audio / video sync and what is required to achieve sync in a generic way (hopefully it's not just use SDL :)). That is a problem well known in playback to be difficult on generic / uncontrolled hardware and OS. Giving people an appreciation for this seems like a good idea, and serves to reinforce how using an OSS library provides benefit here, and why it might make sense to build an open source system on top of this
- A simple walk-throughs of how one might integrate and build a quick viewer in python that can display not only the main beauty image, but also enable easy switching to the AOVs of an EXR multi-part image. and then hit play and see a sequence of that showing off the threading
  - A modification of this might be to use this same beauty and AOV demonstration, and instead show how to use the gstreamer pipeline to apply custom pre-processing / filtering more efficiently prior to streaming into a machine learning training system.

The above are not requirements, just meant as examples of the additional ideas that might be added to make this a good talk / presentation to fill 20-25 minutes and to pique the audience interest.



----------------------- REVIEW 4 ---------------------
SUBMISSION: 6
TITLE: The motion picture industry and open source software: GStreamer as an alternative
AUTHORS: Thibault Saunier

----------- Overall evaluation -----------
SCORE: -1 (weak reject)
----- TEXT:
Building a strong Open Source community for VFX is indeed a very important mission. It looks like GStreamer is a solid and battleground tested Open Source platform that might benefit some Post-Production workflows.

Unfortunately this submission feels more like an advertisement for the platform, rather than a talk about actual or potential use cases. It's also completely lacking an analysis of the advantages / disadvantages over alternative solutions.



----------------------- REVIEW 5 ---------------------
SUBMISSION: 6
TITLE: The motion picture industry and open source software: GStreamer as an alternative
AUTHORS: Thibault Saunier

----------- Overall evaluation -----------
SCORE: -1 (weak reject)
----- TEXT:
The submission presents the open-source framework,"GStreamer" with arguments for its application to the motion picture industry.  In its current form, there is a short, high-level overview of the strengths of the framework, suitable for a developer audience.  Whilst a subset of the DigiPro audience would be able to appreciate the proposal, it doesn't directly speak to the majority of the production community.

I'm not sure that DigiPro is really the most suitable venue to drive the adoption of this open-source software which, as the authors acknowledge, does not have an existing foothold in the industry.  As presented, there is insufficent detail or comparison to encumbent technologies to make a compelling case.  If these details were provided, the result would likely be too technical / detailed / dry for the audience.

