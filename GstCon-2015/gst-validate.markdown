% What is new in GstValidate 1.6
% Thibault Saunier, Collabora Ltd.

# New features

- Valgrind integration
- Concurrent tests run
- Simpler to read reports
- ...

# Plugin system

* ssim
* gtk
* faultinjector

# New tests

- Clear separation between the test launcher and the testsuites implementation
- More tests in the default testsuite
- Dedicated testsuite for filters
- Intensive testsuite for the GStreamer Editing Services
