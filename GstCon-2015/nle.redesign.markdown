% Non Linear Engine: replacement for GnonLin
% Thibault Saunier, Collabora Ltd.

# What does NLE do?

- Time based pipeline sequencing 

![nlecomposition](NleComposition.jpg)

# Lack of thread safety in Gnonlin

- gnlcomposition used to unlink/relink new `pipelines` from the streaming thread (where the EOS was received) or from the seeking thread

- In nlecomposition we introduced a new `master` thread where all the operations on children happen sequentially

# Creation of many useless thread

- In GnlComposition we used to have all elements inside in `PAUSED` state → **many** thread created and just waiting

- In NleComposition elements are **not** inside it until they are actually needed (and are in `READY` state only)
