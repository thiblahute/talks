% GStreamer-sharp: the revival of our .net bindings
% Thibault Saunier

# GstSharp

- Not maintained since 2014 (Gst 1.4)
* Updated to latest master
* Gtk-sharp 3.X was never released and was not maintain since 2014
    * Started a fork aiming at being the new upstream 

# Enhancements

* Bug fixes and testing was done
* Changed the build system to `meson`
* Now integrated with `gst-build`
* Removed the need for glue (in the code generator in gtk-sharp)
* Going to release Gst 1.12 (rsn)
    * Generation of nuggets

# Conclusion

Use it... it works (for me) !
