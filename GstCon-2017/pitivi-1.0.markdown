% Pitivi 1.0: We are getting there!
% Thibault Saunier

# Pitivi

* Video Editing Software based on GStreamer
* Started in 2004....
* Main user of the GStreamer Editing Services

# Generally useful now!

* People are editing complex projects with it
* Some companies start using it for their basic edit needs
* Many bugs and enhancements landed in GStreamer itself to support our use case

<!-- VAAPI -->

# What is next

* Finalize 1.0!
* Already branched 1.0 
    * Master is going to be the next major version
* Better take advantage of the power of GStreamer
    * Nicely expose UIs for effects
    * Hardware acceleration (gl?)
* Master is already extensible through plugins

# Conclusion

```
flatpak install --user --from http://goo.gl/cEVZbt && \
flatpak run org.pitivi.Pitivi
```
