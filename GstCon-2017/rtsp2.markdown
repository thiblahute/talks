% A quick look at RTSP 2.0
% Thibault Saunier

# RTSP 2.0

- New non backward compatible version of the RTSP protocol
* Proposed standard since December 2016
* General cleanup and rewrite of the specification

# New features

* Pipelined requests
* Better extensibility
* Possible notification from server to client (PLAY_NOTIFY)
* Specified IPv6 support

# Removed features

* RECORD method
* Message transport over UDP
* ...

# GStreamer implementation

* Part of our RTSP stack
* Merged in master (will be in 1.14)
* Missing a few features still:
    * Extension handling (no extension specified yet)
    * Server to client notification
