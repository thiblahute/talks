#!/usr/bin/env python
# encoding: utf-8

# Use our scenario
validate.add_scenarios("seek_and_reverse_playback")

# We need to make sure that our scenario files are usable, set the
# GST_VALIDATE_SCENARIOS_PATH environment variable.
os.environ["GST_VALIDATE_SCENARIOS_PATH"] = \
        os.path.join(os.path.dirname(os.path.realpath(__file__)), "scenarios")

validate.add_encoding_formats([MediaFormatCombination("mkv", "ac3", "h264")])

validate.add_generators([validate.GstValidatePlaybinTestsGenerator(validate),
                         validate.GstValidateMediaCheckTestsGenerator(validate),
                         validate.GstValidateTranscodingTestsGenerator(validate)])
